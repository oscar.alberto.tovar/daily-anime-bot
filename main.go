package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/bsdlp/discord-interactions-go/interactions"
	"github.com/gorilla/mux"
)

var (
	pubKey     []byte
	httpClient = &http.Client{Timeout: time.Millisecond * 1500, Transport: http.DefaultTransport}
)

func main() {
	setupKey()
	r := mux.NewRouter()
	r.Use(verifyMiddleware)
	r.HandleFunc("/", homeHandler)
	log.Println("Starting http server...")
	log.Fatal(http.ListenAndServe(":8080", r))
}

func setupKey() {
	pubKeyHex := os.Getenv("APP_PUB_KEY")
	if len(pubKeyHex) == 0 {
		log.Fatal("invalid public key")
	}
	var err error
	pubKey, err = hex.DecodeString(pubKeyHex)
	if err != nil {
		log.Fatalf("failed to decode public key hex err: %v", err)
	}
}

func verifyMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !interactions.Verify(r, pubKey) {
			http.Error(w, "unauthorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var data interactions.Data
	err := json.NewDecoder(r.Body).Decode(&data)
	log.Printf("received interaction req: %#v\n", data)
	if err != nil {
		log.Printf("failed to handle discord interaction err: %v\n", err)
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	if data.Type == interactions.Ping {
		_, err := w.Write([]byte(`{"type":1}`))
		if err != nil {
			log.Printf("failed to respond to discord interaction err: %v\n", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		return
	}

	if data.Type == interactions.ApplicationCommand {
		imgURL, err := getWaifuImageURL()
		if err != nil {
			log.Printf("failed to retrieve waifu image err: %v\n", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		response := &interactions.InteractionResponse{
			Type: interactions.ChannelMessageWithSource,
			Data: &interactions.InteractionApplicationCommandCallbackData{
				Content: imgURL,
			},
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("content-type", "application/json")
		err = json.NewEncoder(w).Encode(response)
		log.Printf("created response: %#v\n", *response)
		if err != nil {
			log.Printf("failed to json encode payload err: %v\n", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		var responsePayload bytes.Buffer
		err = json.NewEncoder(&responsePayload).Encode(response)
		if err != nil {
			log.Printf("failed to json encode payload err: %v\n", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		resp, err := http.Post(data.ResponseURL(), "application/json", &responsePayload)
		if err != nil {
			log.Printf("failed to json encode payload err: %v\n", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()
		buf, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		fmt.Println("discord response", string(buf))
		return
	}
}

type imgResponse struct {
	URL string `json:"url"`
}

func getWaifuImageURL() (string, error) {
	const endpoint = "https://api.waifu.pics/sfw/waifu"
	resp, err := httpClient.Get(endpoint)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	var body imgResponse
	err = json.NewDecoder(resp.Body).Decode(&body)
	if err != nil {
		return "", fmt.Errorf("failed to decode api call response body err: %v", err)
	}
	return body.URL, nil
}
