module gitlab.com/oscar.alberto.tovar/daily-discord-anime

go 1.17

require (
	github.com/bsdlp/discord-interactions-go v0.0.0-20201227083222-a2ba84473ce8
	github.com/gorilla/mux v1.8.0
)
